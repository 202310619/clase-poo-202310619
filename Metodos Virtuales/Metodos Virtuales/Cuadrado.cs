﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metodos_Virtuales
{
    class Cuadrado:Triangulo
    {
        public override void Area(double a, double b)
        {
            double resultado = a * b;
            Console.WriteLine("El area del cuadrado es {0} ", resultado);
        }
    }
}
