﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metodos_Virtuales
{
    class Triangulo
    {
        public virtual void Area(double a,double b)
        {
            double resultado = (a * b) / 2;
            Console.WriteLine("El area del triangulo es {0} ", resultado);
        }
    }
}
