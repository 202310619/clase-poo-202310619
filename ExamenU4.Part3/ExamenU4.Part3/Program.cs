﻿using System;

namespace ExamenU4.Part3
{
    class Program
    {
        static void Main(string[] args)
        {
            Carro obj = new Carro();
            Carro obj2 = new Carro();
            string resultado=obj.Avanzar("7 kilo metros");
            Console.WriteLine("Recorriste en carro : " + resultado);
            obj.Girar(90);
            string resultado2=obj2.Avanzar("2 kilo metros");
            Console.WriteLine("Recorriste en bicicleta : " + resultado2);
            obj2.Girar(90);
        }
    }
}
