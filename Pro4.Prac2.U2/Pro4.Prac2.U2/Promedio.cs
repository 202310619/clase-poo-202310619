﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pro4.Prac2.U2
{
    class Promedio
    {
        int Cal1;
        int Cal2;
        int Cal3;
        int promedio;
        public void CalcPromedio()
        {
            Console.WriteLine("Ingrese su primera calificacion :");
            Cal1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingrese su segunda calificacion :");
            Cal2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingrese su tercera calificacion :");
            Cal3 = Convert.ToInt32(Console.ReadLine());
            promedio = (Cal1 + Cal2 + Cal3)/3;
        }
        public void MostrarPromedio()
        {
            Console.WriteLine("El promedio es de {0}", promedio);
        }
    }
}
