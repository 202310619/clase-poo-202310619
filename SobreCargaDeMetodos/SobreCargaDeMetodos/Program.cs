﻿using System;

namespace SobreCargaDeMetodos
{
    class Program
    {
        static void Main(string[] args)
        {
            Operaciones O = new Operaciones();
            int resultado = O.suma(5, 9);
            Console.WriteLine("Resultado metodo 1: {0}", resultado);
            double resultado2 = O.suma(5.8, 9.7, 3.14);
            Console.WriteLine("Resultado metodo 2: {0}", resultado2);
            string resultado3 = O.suma("1","8");
            Console.WriteLine("Resultado metodo 3: {0}", resultado3);
            float resultado4 = O.suma(3.9f, 8.9f, 8.9f);
            Console.WriteLine("Resultado metodo 4: {0}", resultado4);
        }
    }
}
