﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pro3.Prac3.U2
{
    class Primo
    {
        public int n = 0;

        public void primo()
        {
            Console.WriteLine("Ingrese un numero");
            n = Convert.ToInt32(Console.ReadLine());
        }
        public void ImprimePrimo()
        {
            if (n % 2 == 1)
            {
                Console.WriteLine("El numero es un numero primo");
            }
            else
            {
                Console.WriteLine("No es un numero primo");
            }
        }
        ~ Primo()
        {
            this.n = 0;
            Console.WriteLine("Destruyendo...");
        }
    }
}
