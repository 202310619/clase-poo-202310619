﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pro6.Prac4.U2
{
    class Ordenar
    {
        public void Llenar(short[]a)
        {
            short ad = 10000;
            for (int k = 0; k < a.Length; k++) 
            {
                Console.WriteLine("Ingrese un valor, posicion ({0})", k);
                a[k] = Convert.ToInt16(Console.ReadLine());
            }
        }
        public void Llenar(long[] a)
        {
            for (int k = 0; k < a.Length; k++)
            {
                Console.WriteLine("Ingrese un valor, posicion ({0})", k);
                a[k] = Convert.ToInt64(Console.ReadLine());
            }
        }
        public void DetectarMayor(short[] a)
        {
            short max = 0;
            int id = 0;
            for (int k = 0; k < a.Length; k++)
            {
                if (a[k]>max)
                {
                    max = a[k];
                    id = k;
                }
            }
            Console.WriteLine("El numero mayor es {0} y se encuentra en la posiciones {1}");
            Console.WriteLine();
        }
        public void DetectarMayor(long[] a)
        {
            long max = 0;
            int id = 0;
            for (int k = 0; k < a.Length; k++)
            {
                if (a[k] > max)
                {
                    max = a[k];
                    id = k;
                }
            }
            Console.WriteLine("El numero mayor es {0} y se encuentra en la posiciones {1}");
        }

    }
}
