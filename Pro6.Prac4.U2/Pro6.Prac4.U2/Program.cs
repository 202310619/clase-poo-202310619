﻿using System;

namespace Pro6.Prac4.U2
{
    class Program
    {
        static void Main(string[] args)
        {
            Ordenar Or = new Ordenar();
            short[] a = new short[20];
            long[] b = new long[20];
            Or.Llenar(a);
            Or.Llenar(b);
            Or.DetectarMayor(a);
            Or.DetectarMayor(b);
        }
    }
}
