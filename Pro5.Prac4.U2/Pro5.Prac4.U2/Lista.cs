﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pro5.Prac4.U2
{
    class Lista
    {
        public void Ordenar(int[] edad )
        {
            int T = 0;
            for (int i=0;i<10;i++)
            {
                for (int j = 0; j < 9-1; j++)
                {
                    if(edad[j]>edad[j+1])
                    {
                        T = edad[j];
                        edad[j] = edad[j + 1];
                        edad[j + 1] = T;
                    }
                }
            }
            for (int i=1;i<edad.Length;i++)
            {
                Console.WriteLine(edad[i]);
            }
        }
        public void Ordenar(float[]edad)
        {
            float T = 0;
            for (int i = 1; i < 10; i++)
            {
                for (int j = 1; j < 9 ; j++)
                {
                    if (edad[j] > edad[j + 1])
                    {
                        T = edad[j];
                        edad[j] = edad[j + 1];
                        edad[j + 1] = T;
                    }
                }
            }
            for (int i = 1; i < edad.Length; i++)
            {
                Console.WriteLine(edad[i]);
            }
        }
        public void MostrarNormal(int[]n)
        {
            for (int i = 0; i<10; i++)
            {
                Console.WriteLine(n[i]);
            }
        }
        public void MostrarNormal(float[] n)
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(n[i]);
            }
        }
    }
}
