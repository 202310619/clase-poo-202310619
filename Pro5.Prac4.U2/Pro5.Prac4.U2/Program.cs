﻿using System;

namespace Pro5.Prac4.U2
{
    class Program
    {
        static void Main(string[] args)
        {
            float[] Lista1 = new float[10];
            Lista1[0] = 8.8f;
            Lista1[1] = 5.5f;
            Lista1[2] = 2.3f;
            Lista1[3] = 9.4f;
            Lista1[4] = 7.7f;
            Lista1[5] = 6.43f;
            Lista1[6] = 1f;
            Lista1[7] = 3.2f;
            Lista1[8] = 4.4f;
            Lista1[9] = 9f;
            int[] Lista2 = new int[10];
            Lista1[0] = 8f;
            Lista1[1] = 5f;
            Lista1[2] = 2f;
            Lista1[3] = 9f;
            Lista1[4] = 7f;
            Lista1[5] = 6f;
            Lista1[6] = 1f;
            Lista1[7] = 3f;
            Lista1[8] = 4f;
            Lista1[9] = 9f;
            Lista Li = new Lista();
            Li.MostrarNormal(Lista1);
            Li.Ordenar(Lista1);
            Li.MostrarNormal(Lista2);
            Li.Ordenar(Lista2);
        }
    }
}
