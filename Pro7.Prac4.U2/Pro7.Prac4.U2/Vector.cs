﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pro7.Prac4.U2
{
    class Vector
    {
        public int Ceros = 0; 
        public int Negativos = 0;
        public int Positivos = 0;
        public int Suman = 0;
        public int SumPos = 0;
        public void Contador(int[]n)
        {
            Random rd = new Random();
            for (int i=0;i<300;i++)
            {
                n[i] = rd.Next(-30, 30);
            }
        }
        public void Cero(int[] n)
        {
            for (int i = 0; i < 300; i++)
            {
                if(n[i]==0)
                {
                    Ceros++;
                }
            }
        }
        public void Negativo(int[] n)
        {
            for (int i = 0; i < 300; i++)
            {
                if (n[i] == 0)
                {
                    Negativos++;
                    Suman += n[i];
                }
            }
        }
        public void Positivo(int[] n)
        {
            for (int i = 0; i < 300; i++)
            {
                if (n[i] == 0)
                {
                    Positivos++;
                    SumPos += n[i];
                }
            }
        }
        public void VerArreglo(int[] n)
        {
            int iterator = 0;
            for (int i = 0; i < 300; i++)
            {
                if (n[i] == 0)
                {
                    if (iterator<30)
                    {
                        Console.Write("  " + n[i] + "");
                        iterator++;
                    }
                    else
                    {
                        Console.Write("  " + n[i] + "");
                        iterator = 0;
                    }
                }
                else
                {
                    if (iterator < 30)
                    {
                        Console.Write("  " + n[i] + "");
                        iterator++;
                    }
                    else
                    {
                        Console.Write("  " + n[i] + "");
                        iterator = 0;
                    }
                }
            }
        }
    }
}
