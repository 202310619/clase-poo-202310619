﻿using System;

namespace Pro7.Prac4.U2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] n = new int[300];
            Vector Vec = new Vector();
            Vec.Contador(n);
            Vec.Cero(n);
            Vec.Positivo(n);
            Vec.Negativo(n);
            Console.WriteLine("Ceros totales: {0}", Vec.Ceros);
            Console.WriteLine();
            Console.WriteLine("Negativos totales: {0}", Vec.Negativos);
            Console.WriteLine();
            Console.WriteLine("Positivos totales: {0}", Vec.Positivos);
            Console.WriteLine();
            Console.WriteLine("Suma total de numeros negativos: {0}", Vec.Suman);
            Console.WriteLine();
            Console.WriteLine("Suma total de numeros positivos: {0}", Vec.SumPos);
            Console.WriteLine();
        }
    }
}
