﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen_Practico_U1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Vaso V1 = new Vaso();
            V1.settamanio(10);
            int tamanioV1 = V1.gettamanio();
            MessageBox.Show("El tamaño del vaso es:" + tamanioV1.ToString()+"cm");
            V1.setancho(5);
            int anchoV1 = V1.getancho();
            MessageBox.Show("El ancho del vaso es:" + anchoV1.ToString()+"cm");
            V1.setcontenido(1);
            int contenidoV1 = V1.getcontenido();
            MessageBox.Show("El contenido del vaso es de:" + contenidoV1.ToString() + "litro");
            V1.setcolor("Azul");
            string colorV1 = V1.getcolor();
            MessageBox.Show("El color del vaso es:" + colorV1.ToString());
        }
    }
}
