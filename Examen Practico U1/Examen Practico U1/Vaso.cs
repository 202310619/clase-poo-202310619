﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_Practico_U1
{
    class Vaso
    {
        public int tamanio = 0;
        public string color = "";
        public int contenido = 0;
        public int ancho = 0;
        public void settamanio(int t)
        {
            this.tamanio = t;
        }
        public int gettamanio()
        {
            return this.tamanio;
        }
        public void setcontenido(int cn)
        {
            this.contenido = cn;
        }
        public int getcontenido()
        {
            return this.contenido;
        }
        public void setancho(int a)
        {
            this.ancho = a;
        }
        public int getancho()
        {
            return this.ancho;
        }
        public void setcolor(string c)
        {
            this.color = c;
        }
        public string getcolor()
        {
            return this.color;
        }
    }
}
