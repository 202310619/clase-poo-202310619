﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pro10.Prac2.U2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            metodo();
        }
        public void metodo()
        {
            double C1 = Convert.ToDouble(textBox1.Text);
            double C2 = Convert.ToDouble(textBox2.Text);
            double C3 = Convert.ToDouble(textBox3.Text);
            double C4 = Convert.ToDouble(textBox4.Text);
            double C5 = Convert.ToDouble(textBox5.Text);
            double C6 = Convert.ToDouble(textBox6.Text);
            double promedio = (C1 + C2 + C3 + C4 + C5 + C6) / 6;
            PRO.Text = promedio.ToString();
        }
    }
}
