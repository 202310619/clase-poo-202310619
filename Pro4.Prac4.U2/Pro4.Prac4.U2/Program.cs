﻿using System;

namespace Pro4.Prac4.U2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("---Escuela BuenaVentura---");
            string[] nombres = new string[35];
            float[] telefonos = new float[35];
            Datos Da = new Datos();
            Da.LlenarNombres(nombres);
            Da.LlamarTelefonos(telefonos);
            Da.ImprimirDatos(nombres, telefonos);
        }
    }
}
