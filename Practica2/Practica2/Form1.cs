﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnClase_Click(object sender, EventArgs e)
        {
            tv Tv1 = new tv();
            Tv1.settamanio(30);
            int tamanioTv1 = Tv1.gettamanio();
            MessageBox.Show("El tamaño de la Tv1 es:"+tamanioTv1.ToString());
            Tv1.setvolumen(10);
            int volumenTv1 = Tv1.getvolumen();
            MessageBox.Show("El volumen de la Tv1 es:" + volumenTv1.ToString());
            Tv1.setcolor("negro");
            string colorTv1 = Tv1.getcolor();
            MessageBox.Show("El color de la Tv1 es:" + colorTv1.ToString());
            Tv1.setbrillo(25);
            int brilloTv1 = Tv1.getbrillo();
            MessageBox.Show("El brillo de la Tv1 es:" + brilloTv1.ToString());
            Tv1.setcontraste(20);
            int contrasteTv1 = Tv1.getcontraste();
            MessageBox.Show("El contraste de la Tv1 es:" + contrasteTv1.ToString());
            Tv1.setmarca("LG");
            string marcaTv1 = Tv1.getmarca();
            MessageBox.Show("La marca de la Tv1 es:" + marcaTv1.ToString());
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
