﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica2
{
    class tv
    {
        public int tamanio = 0;
        public int volumen = 0;
        public string color = "";
        public int brillo = 0;
        public int contraste = 0;
        public string marca = "";
        public void settamanio(int t)
        {
            this.tamanio = t;
        }
        public int gettamanio()
        {
            return this.tamanio;
        }
        public void setvolumen(int volumen)
        {
            this.volumen = volumen;
        }
        public int getvolumen()
        {
            return this.volumen;
        }
        public void setcolor(string color)
        {
            this.color = color;
        }
        public string getcolor()
        {
            return this.color;
        }
        public void setbrillo(int brillo)
        {
            this.brillo=brillo;
        }
        public int getbrillo()
        {
            return this.brillo;
        }
        public void setcontraste(int contraste)
        {
            this.contraste=contraste;
        }
        public int getcontraste()
        {
            return this.contraste;
        }
        public void setmarca(string marca)
        {
            this.marca=marca;
        }
        public string getmarca()
        {
            return this.marca;
        }
    }
}
