﻿using System;

namespace Abstracto
{
    class Program
    {
        static void Main(string[] args)
        {
            Avion Av = new Avion();
            AutoMovil Au = new AutoMovil();
            Av.Mantenimiento();
            Au.Mantenimiento();
        }
    }
}
