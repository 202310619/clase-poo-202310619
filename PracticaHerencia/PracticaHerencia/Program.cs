﻿using System;

namespace PracticaHerencia
{
    class Program
    {
        static void Main(string[] args)
        {
            //Se manda a llamar la clase hijo
            Hijo obj = new Hijo();
            obj.metodo1();
            obj.AccesoMetodo3();
            //Aqui se manda a llamar la clase Herencia
            Herencia obj2 = new Herencia();
            obj2.AccesoMetodo2();
            Console.ReadKey();
        }
    }
}
