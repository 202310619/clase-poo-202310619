﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticaHerencia
{
    class Herencia
    {
        public int atributo1;
        private int atributo2;
        protected int atributo3;
        public void metodo1()
        {
            Console.WriteLine("Este es el metodo 1 de la clase Herencia");
        }
        private void metodo2()
        {
            Console.WriteLine("Este es el metodo 2 de la clase Herencia no se puede heredar");
        }
        protected void metodo3()
        {
            Console.WriteLine("Este es el metodo 3 de la clase Herencia Protected");
        }
        //Se crea este metodo public dentro de la clase herencia para poder mandar a llamar un metodo private
        public void AccesoMetodo2()
        {
            metodo2();
        }
    }
    class Hijo:Herencia
    {
        //Se crea un metodo public dentro de la clase hijo para poder encapsular un metodo protected de la clase herencia
        public void AccesoMetodo3()
        {
            metodo3();
        }
    }
    
}
