﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenU4.Part2
{
    class Helado:Heladeria
    {
        public string sh;
        public string sp;
        public override void SaboresDeHelado()
        {
            Console.WriteLine("*-Los sabores de Helado que hay son-*\n-Chocolate\n-Vainilla\n-Nuez\n-Pistache\n-Napolitano\n-Oreo");
            Console.WriteLine("¿Que sabor quiere elegir?");
            sh = Convert.ToString(Console.ReadLine());
        }
        public override void SaboresDePaletas()
        {
            Console.WriteLine("*-Los sabores de Paletas que hay son-*\n-Chocolate\n-Vainilla\n-Fresa\n-Limon\n-Chicle\n-Mango con Chile\n-Esquimales\n-Tamarindo");
            Console.WriteLine("¿Que sabor quiere elegir?");
            sp = Convert.ToString(Console.ReadLine());
        }
        public void Despedida()
        {
            Console.WriteLine("Gracias por comprar su helado sabor  " +  sh + "  Y su paleta sabor  " + sp);
            Console.WriteLine("Vuelva pronto");
        }
    }
}
