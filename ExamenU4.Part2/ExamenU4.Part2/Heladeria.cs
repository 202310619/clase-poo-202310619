﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenU4.Part2
{
    abstract class Heladeria
    {
        public abstract void SaboresDeHelado();
        public abstract void SaboresDePaletas();
    }
}
