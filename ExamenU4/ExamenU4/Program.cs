﻿using System;

namespace ExamenU4
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 Obj = new Class1();
            int resultado=Obj.area(7,7);
            Console.WriteLine("El area del cuadrado es : {0}", resultado);
            double resultado2=Obj.area(10.5,5.5);
            Console.WriteLine("El area del rectangulo es : {0}", resultado);
        }
    }
}
