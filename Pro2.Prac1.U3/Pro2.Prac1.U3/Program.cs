﻿using System;

namespace Pro2.Prac1.U3
{
    class Program
    {
        static void Main(string[] args)
        {
            Aerolinea Ae = new Aerolinea();
            Ae.Reservacion();
            Vuelvo Vu = new Vuelvo();
            Vu.Destino();
            Vu.Salida();
            Vu.FechaDelVuelo();
            Vu.HoraDelVuelo();
            Vu.PrecioDelVuelo();
            Vu.NumeroDelVuelo();
            Vu.MostrarDatos();
        }
    }
}
