﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pro3.Prac4.U2
{
    class Cuadrado
    {
        public void Elevar(int n)
        {
            Console.WriteLine("Ingrese un numero entero para elevarlo");
            n = int.Parse(Console.ReadLine());
            Console.WriteLine("Tipo entero");
            Console.WriteLine("    " + n + "=" + Math.Pow(n, 2));
            Console.WriteLine("---------");
        }
        public void Elevar(double n)
        {
            Console.WriteLine("Ingrese un numero decimal para elevarlo");
            n = double.Parse(Console.ReadLine());
            Console.WriteLine("Tipo double");
            Console.WriteLine("    " + n + "=" + Math.Pow(n, 2));
        }
    }
}
