﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Circunferencia.Prac2
{
    class Operacion
    {
        private int r, r2;
        private double pin=3.14;
        public void area()
        {
            double area;
            Console.WriteLine("Introduce el valor de la circunferencia :");
            r = int.Parse(Console.ReadLine());
            r2 = r * r;
            area = r2 * pin;
            Console.WriteLine("El area es:" + area);
        }
        public void perimetro()
        {
            double perimetro;
            perimetro = 2 * r * pin;
            Console.WriteLine("El perimetro es:" + perimetro);
        }
        
        
    }
}
