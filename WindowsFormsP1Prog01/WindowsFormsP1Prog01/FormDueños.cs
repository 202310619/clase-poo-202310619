﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsP1Prog01
{
    public partial class FormDueños : Form
    {
        public FormDueños()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            Dueños Objd = new Dueños();
            int HorasTrabajadasI = int.Parse(txtHtrabajadas.Text);
            int HorasExtrasI = int.Parse(txtHextras.Text);

            Objd.SalarioSemanal(HorasExtrasI, HorasTrabajadasI);
            lblSsemanal.Text = "El salario semanal es: " + Objd.X;
        }
    }
}
