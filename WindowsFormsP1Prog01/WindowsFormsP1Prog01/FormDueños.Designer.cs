﻿
namespace WindowsFormsP1Prog01
{
    partial class FormDueños
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSsemanal = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtHextras = new System.Windows.Forms.TextBox();
            this.txtHtrabajadas = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblSsemanal
            // 
            this.lblSsemanal.AutoSize = true;
            this.lblSsemanal.Location = new System.Drawing.Point(43, 228);
            this.lblSsemanal.Name = "lblSsemanal";
            this.lblSsemanal.Size = new System.Drawing.Size(83, 13);
            this.lblSsemanal.TabIndex = 34;
            this.lblSsemanal.Text = "Salario Semanal";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Ingrese Horas Extras";
            // 
            // txtHextras
            // 
            this.txtHextras.Location = new System.Drawing.Point(198, 159);
            this.txtHextras.Name = "txtHextras";
            this.txtHextras.Size = new System.Drawing.Size(100, 20);
            this.txtHextras.TabIndex = 32;
            // 
            // txtHtrabajadas
            // 
            this.txtHtrabajadas.Location = new System.Drawing.Point(198, 89);
            this.txtHtrabajadas.Name = "txtHtrabajadas";
            this.txtHtrabajadas.Size = new System.Drawing.Size(100, 20);
            this.txtHtrabajadas.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Ingresa Horas Trabajadas";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(168, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 20);
            this.label1.TabIndex = 29;
            this.label1.Text = "Dueños";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(328, 242);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 28;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // FormDueños
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 303);
            this.Controls.Add(this.lblSsemanal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtHextras);
            this.Controls.Add(this.txtHtrabajadas);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCalcular);
            this.Name = "FormDueños";
            this.Text = "FormDueños";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSsemanal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtHextras;
        private System.Windows.Forms.TextBox txtHtrabajadas;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCalcular;
    }
}