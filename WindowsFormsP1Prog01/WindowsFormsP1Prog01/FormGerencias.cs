﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsP1Prog01
{
    public partial class FormGerencias : Form
    {
        public FormGerencias()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            Gerencias Objg = new Gerencias();
            int HorasTrabajadasI = int.Parse(txtHtrabajadas.Text);
            int HorasExtrasI = int.Parse(txtHextras.Text);

            Objg.SalarioSemanal(HorasExtrasI, HorasTrabajadasI);
            lblSsemanal.Text = "El salario semanal es: " + Objg.X;
        }
    }
}
