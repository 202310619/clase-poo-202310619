﻿using System;

namespace EjemploArreglos
{
    class Program
    {
        static void Main(string[] args)
        {
            int suma = 0;
            int[] CatalogoNumeros = new int[10];
            for (int i=0; i<CatalogoNumeros.Length; i++)
            {
                Console.WriteLine("Ingresa un numero :");
                int num = Convert.ToInt32(Console.ReadLine());
                CatalogoNumeros[i] = num;
            }
            Console.WriteLine("");
            foreach (int val in CatalogoNumeros)
            {
                Console.WriteLine(val);
                suma += val;
            }
            Console.WriteLine("");
            Console.WriteLine(suma);
            Console.WriteLine("");
            Console.WriteLine(CatalogoNumeros.Length);
            Console.ReadKey();
        }
    }
}
