﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen.U3
{
    class Class1
    {
        public int c1; 
        public int c2;
        public int c3;
        public int c4;
        public int c5;
        public int c6;
        protected double promedio;
        public Class1()
        {
      
            this.promedio=0;
        }
        public void Metodo1()
        {
            Console.WriteLine("Boleta de calificaiones");
            Console.WriteLine("");
            Console.WriteLine("Ingrese su calificacion de Programacion Orientada a Objetos");
            c1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingrese su calificacion de Calculo");
            c2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingrese su calificacion de Quimica");
            c3 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingrese su calificacion de Algebra Lineal");
            c4 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingrese su calificacion de Contabilidad Financiera");
            c5 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingrese su calificacion de Probabilidad y Estadistica");
            c6 = Convert.ToInt32(Console.ReadLine());
            promedio = (c1 + c2 + c3 + c4 + c5 + c6) / 6;
        }
        private void Metodo2()
        {
            Console.WriteLine("El promedio total de sus califiaciones es:{0}", promedio);
        }
        protected void AccesoMetodo2()
        {
            Metodo2();
        }
        ~Class1()
        {
            System.Console.WriteLine("Destruyendo.....");
        }
        
    }
    class Class2 : Class1
    {
        public Class2()
        {

        }
        public void accesometodos()
        {
         AccesoMetodo2();
        }
        ~Class2()
        {
            System.Console.WriteLine("Destruyendo....");
        }
    }
}
