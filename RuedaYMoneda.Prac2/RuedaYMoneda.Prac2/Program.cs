﻿using System;

namespace RuedaYMoneda.Prac2
{
    class Program
    {
        static void Main(string[] args)
        {
            Rueda r = new Rueda();
            r.area();
            r.perimetro();
            Moneda m = new Moneda();
            m.area();
            m.perimetro();
            Console.ReadLine();
        }
    }
}
