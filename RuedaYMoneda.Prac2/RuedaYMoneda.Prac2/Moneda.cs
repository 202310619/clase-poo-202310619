﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RuedaYMoneda.Prac2
{
    class Moneda
    {
        private int r = 1;
        private int r2;
        private double pin = 3.14;
        public void area()
        {
            double area;
            r2 = r * r;
            area = r2 * pin;
            Console.WriteLine("El area de la moneda es:" + area);
        }
        public void perimetro()
        {
            double perimetro;
            perimetro = 2 * r * pin;
            Console.WriteLine("El perimetro de la moneda es:" + perimetro);
        }
    }
}
