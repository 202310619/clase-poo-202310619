﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RuedaYMoneda.Prac2
{
    class Rueda
    {
        private int r = 10;
        private int r2;
        private double pin = 3.14;
        public void area()
        {
            double area;
            r2 = r * r;
            area = r2 * pin;
            Console.WriteLine("El area de la rueda es:" + area);
        }
        public void perimetro()
        {
            double perimetro;
            perimetro = 2 * r * pin;
            Console.WriteLine("El perimetro de la rueda es:" + perimetro);
        }
    }
}
